package es.npatarino.android.gotchallenge.customs;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by antoniohormigo on 4/3/16.
 */
public class Utils {

    public static final String THE_JSON = "theJson";

    public static SharedPreferences getDefaultSharedPreferences(Context context) {
        return context.getSharedPreferences("MisPreferencias",Context.MODE_PRIVATE);
    }

    public static String getJsonString(Context ctx, String url) throws IOException {
        URL obj;
        String stringJson;
        try {
            obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            getDefaultSharedPreferences(ctx).edit()
                    .putString("theJson", response.toString()).apply();

            stringJson = response.toString();

        } catch (UnknownHostException e) {
            stringJson = getDefaultSharedPreferences(ctx).getString(THE_JSON, "");
        }
        return stringJson;
    }

}
