package es.npatarino.android.gotchallenge.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.npatarino.android.gotchallenge.model.GoTCharacter;
import es.npatarino.android.gotchallenge.R;
import es.npatarino.android.gotchallenge.adapters.GoTHouseAdapter;
import es.npatarino.android.gotchallenge.customs.Utils;
import es.npatarino.android.gotchallenge.interfaces.ListFragment;

/**
 * Created by antoniohormigo on 4/3/16.
 */
public class GoTHousesListFragment extends Fragment implements ListFragment {

    private static final String TAG = "GoTHousesListFragment";

    public GoTHousesListFragment() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        final ContentLoadingProgressBar pb = (ContentLoadingProgressBar) rootView.findViewById(R.id.pb);
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv);

        final GoTHouseAdapter adp = new GoTHouseAdapter(getActivity());
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setHasFixedSize(true);
        rv.setAdapter(adp);

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String stringJson = Utils.getJsonString(getActivity(), getString(R.string.url));

                    Type listType = new TypeToken<ArrayList<GoTCharacter>>() {
                    }.getType();

                    final List<GoTCharacter> finalChars = new ArrayList<>();
                    if (stringJson != null && stringJson.length() > 0) {
                        final List<GoTCharacter> characters = new Gson().fromJson(stringJson, listType);
                        if (characters != null) {
                            finalChars.addAll(characters);
                        }
                    }
                    GoTHousesListFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrayList<GoTCharacter.GoTHouse> hs = new ArrayList<GoTCharacter.GoTHouse>();
                            for (int i = 0; i < finalChars.size(); i++) {
                                boolean b = false;
                                for (int j = 0; j < hs.size(); j++) {
                                    if (hs.get(j).getN().equalsIgnoreCase(finalChars.get(i).getHn())) {
                                        b = true;
                                    }
                                }
                                if (!b) {
                                    if (finalChars.get(i).getHi() != null && !finalChars.get(i).getHi().isEmpty()) {
                                        GoTCharacter.GoTHouse h = new GoTCharacter.GoTHouse();
                                        h.setI(finalChars.get(i).getHi());
                                        h.setN(finalChars.get(i).getHn());
                                        h.setU(finalChars.get(i).getHu());
                                        hs.add(h);
                                        b = false;
                                    }
                                }
                            }
                            adp.addAll(hs);
                            adp.notifyDataSetChanged();
                            pb.hide();
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }
        }).start();
        return rootView;
    }

    @Override
    public void applyFilter(String text) {

    }

    @Override
    public void cancelFilter() {

    }

    @Override
    public Fragment getFragment() {
        return this;
    }
}