package es.npatarino.android.gotchallenge.interfaces;

import android.support.v4.app.FragmentManager;

/**
 * Created by antoniohormigo on 5/3/16.
 */
public interface ListActivity {

    FragmentManager getSupportFragMngr();

}
