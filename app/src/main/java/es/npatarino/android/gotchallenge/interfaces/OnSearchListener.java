package es.npatarino.android.gotchallenge.interfaces;

/**
 * Created by antoniohormigo on 4/3/16.
 */
public interface OnSearchListener {

    void onSearch(String textToSearch);
    void onCancel();

}
