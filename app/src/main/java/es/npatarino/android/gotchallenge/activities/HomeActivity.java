package es.npatarino.android.gotchallenge.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import es.npatarino.android.gotchallenge.R;
import es.npatarino.android.gotchallenge.adapters.SectionsPagerAdapter;
import es.npatarino.android.gotchallenge.interfaces.OnSearchListener;

public class HomeActivity extends GotChallengeActivity {

    SectionsPagerAdapter spa;
    ViewPager vp;
    Toolbar toolbar;
    Toolbar toolbarSearch;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        initToolbar(toolbar);

        setSpa(new SectionsPagerAdapter(this, getSupportFragmentManager()));

        setVp((ViewPager) findViewById(R.id.container));
        getVp().setAdapter(getSpa());

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(getVp());

        // toolbar search
        toolbarSearch = (Toolbar) findViewById(R.id.toolbarSearcher);

        initToolbarSearcher(toolbarSearch, new OnSearchListener() {
            @Override
            public void onSearch(String textToSearch) {
                spa.applyFilter(textToSearch);
            }

            @Override
            public void onCancel() {
                spa.cancelFilter();
            }
        });
    }

    public SectionsPagerAdapter getSpa() {
        return spa;
    }

    public void setSpa(SectionsPagerAdapter spa) {
        this.spa = spa;
    }

    public ViewPager getVp() {
        return vp;
    }

    public void setVp(ViewPager vp) {
        this.vp = vp;
    }

}
